package nl.tsg.broker.participantdescription

import de.fraunhofer.iais.eis.ParticipantNotificationMessage
import de.fraunhofer.iais.eis.RejectionReason
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.StringMessageHandler
import nl.tsg.broker.config.BrokerConfig
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.function.Consumer
import javax.annotation.PostConstruct

@Component
class ParticipantDescriptionHandler(
  private val brokerConfig: BrokerConfig,
  private val responseBuilder: ResponseBuilder,
  private val participantDescriptionPersister: ParticipantDescriptionPersister
) : StringMessageHandler<ParticipantNotificationMessage> {

  @PostConstruct
  fun postConstruct() {
    brokerConfig.initParticipants.forEach(Consumer { participant: String ->
      try {
        participantDescriptionPersister.ingestParticipantDescription(participant)
      } catch (e: IOException) {
        e.printStackTrace()
      }
    })
  }

  override fun handle(header: ParticipantNotificationMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    try {
      participantDescriptionPersister.ingestParticipantDescription(payload ?: "")
      return responseBuilder.createMessageProcessedResponse(header)
    } catch (e: IOException) {
      LOG.error("IOException in processing Connector Description. ", e)
    }
    return responseBuilder.createRejectionResponse(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.INTERNAL_SERVER_ERROR)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ParticipantDescriptionHandler::class.java)
  }
}