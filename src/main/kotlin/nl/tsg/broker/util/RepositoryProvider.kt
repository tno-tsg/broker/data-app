package nl.tsg.broker.util

import de.fraunhofer.iais.eis.util.ConstraintViolationException
import de.fraunhofer.iais.eis.util.VocabUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.eclipse.rdf4j.model.Resource
import org.eclipse.rdf4j.model.Statement
import org.eclipse.rdf4j.query.TupleQueryResult
import org.eclipse.rdf4j.repository.Repository
import org.eclipse.rdf4j.repository.util.Repositories
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.io.IOException
import java.util.*
import java.util.function.Function

abstract class RepositoryProvider(private val httpClient: CloseableHttpClient) {
  protected val LOG = LoggerFactory.getLogger(RepositoryProvider::class.java)
  protected var sparqlUrl: String? = null
  var repository: Repository? = null
  val contextIds: Collection<Resource>
    get() {
      val ret: MutableCollection<Resource> = HashSet()
      repository!!.connection.use { repCon ->
        val result = repCon.contextIDs
        while (result.hasNext()) {
          ret.add(result.next())
        }
      }
      return ret
    }

  @Synchronized fun replaceStatements(context: Resource?, newStatements: Iterable<Statement?>? = null) {
    repository!!.connection.use { repCon ->
      repCon.begin()
      repCon.remove(repCon.getStatements(null, null, null, context), context)
      newStatements?.let {
        repCon.add(newStatements, context)
      }
      repCon.commit()
    }
  }

  fun <T : Any> query(query: String?, processFunction: Function<TupleQueryResult, T>?): T {
    return Repositories.tupleQuery(repository, query, processFunction)
  }

  fun <T : Any> query(query: String?, clazz: Class<T>): List<T> {
    val httpPost = HttpPost(sparqlUrl)
    httpPost.setHeader("Content-Type", "application/sparql-query")
    httpPost.setHeader("Accept", "application/ld+json")
    httpPost.entity = StringEntity(query, ContentType.create("application/sparql-query"))
    try {
      httpClient.execute(httpPost).use { response ->
        val resultJsonLD = String(response.entity.content.readAllBytes())
        val result = JSONObject(resultJsonLD)
        return fusekiJsonLD(result, clazz)
      }
    } catch (e: IOException) {
      LOG.warn("Cannot contact SPARQL url: {}", e.message)
      return emptyList()
    }
  }

  fun queryPlain(query: String?): String {
    val httpPost = HttpPost(sparqlUrl)
    httpPost.setHeader("Content-Type", "application/sparql-query")
    httpPost.setHeader("Accept", "application/ld+json, application/rdf+json, application/json")
    httpPost.entity = StringEntity(query, ContentType.create("application/sparql-query"))
    try {
      httpClient.execute(httpPost).use { response -> return String(response.entity.content.readAllBytes()) }
    } catch (e: IOException) {
      LOG.warn("Cannot contact SPARQL url: {}", e.message)
      return ""
    }
  }

  fun <T : Any> fusekiJsonLD(json: JSONObject, clazz: Class<T>): List<T> {
    val graphs: MutableMap<String, JSONObject?> = HashMap()
    val childGraphs: MutableList<String> = ArrayList()
    if (!json.has("@graph")) {
      return emptyList()
    }
    json.getJSONArray("@graph").filterIsInstance<JSONObject>().forEach { jsonObject ->
      graphs[jsonObject.getString("@id")] = jsonObject
    }
    graphs.values.forEach{ jsonObject -> combineGraps(jsonObject, graphs, childGraphs) }
    childGraphs.forEach { key -> graphs.remove(key) }


    return graphs.values.mapNotNull { graph ->
      graph?.put("@context", json.getJSONObject("@context"))
      try {
        val instance = SerializationHelper.getInstance().fromJsonLD(graph.toString(), clazz)
        VocabUtil().validate(instance)
        instance
      } catch (e: IOException) {
        LOG.info("Cannot parse JSON into {}: {}", clazz.name, e.message)
        null
      } catch (e: ConstraintViolationException) {
        LOG.info("Cannot parse JSON into {}: {}", clazz.name, e.message)
        null
      }
    }
  }

  fun combineGraps(jsonObject: Any?, graphs: Map<String, JSONObject?>, childGraphs: MutableList<String>) {
    when (jsonObject) {
      is JSONObject -> {
        jsonObject.keySet().filter { k: String? -> !referenceByUri.contains(k) }.forEach { s: String ->
          val o = jsonObject[s]
          if (s == "@type") {
            if (o is JSONArray) {
              jsonObject.put(s, o[0])
              return@forEach
            }
          }
          if (s != "@id") {
            if (o is JSONObject || o is JSONArray) {
              combineGraps(o, graphs, childGraphs)
            }
            if (o is String) {
              if (graphs.containsKey(o)) {
                childGraphs.add(o)
                jsonObject.put(s, graphs[o])
              }
            }
          }
        }
      }
      is JSONArray -> {
        val changes = jsonObject.map { o: Any? ->
          if (o is JSONObject || o is JSONArray) {
            combineGraps(o, graphs, childGraphs)
          }
          if (o is String) {
            if (graphs.containsKey(o)) {
              childGraphs.add(o)
              return@map graphs[o]
            }
          }
          o
        }
        jsonObject.toList().forEach{ _ -> jsonObject.remove(0) }
        changes.forEach { value -> jsonObject.put(value) }
      }
      is MutableList<*> -> {
        val changes = jsonObject.map { o: Any? ->
          if (o is JSONObject || o is JSONArray) {
            combineGraps(o, graphs, childGraphs)
          }
          if (o is String) {
            if (graphs.containsKey(o)) {
              childGraphs.add(o)
              return@map graphs[o]
            }
          }
          o
        }

        jsonObject.clear()
        changes.forEach { change -> (jsonObject as JSONArray).put(change) }
      }
    }
  }

  val size: Long
    get() {
      repository!!.connection.use { repCon -> return repCon.size() }
    }

  companion object {
    private val referenceByUri = java.util.List.of(
      "maintainer",
      "curator",
      "connector",
      "parameter",
      "theme",
      "contentStandard",
      "provider",
      "consumer",
      "representationStandard"
    )
  }
}