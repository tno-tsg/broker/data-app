package nl.tsg.broker.util

import org.apache.commons.io.IOUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.Resource
import org.eclipse.rdf4j.model.Statement
import org.eclipse.rdf4j.model.vocabulary.RDF
import org.eclipse.rdf4j.rio.RDFFormat
import org.eclipse.rdf4j.rio.RDFParseException
import org.eclipse.rdf4j.rio.Rio
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class RdfConverter {
  private val logger = LoggerFactory.getLogger(RdfConverter::class.java)

  inner class ConnectorModel(val model: Model, val namedGraph: Resource?)

  @Throws(IOException::class)
  fun toRdf(connectorSelfDescription: String): ConnectorModel {
    val model = parse(connectorSelfDescription, RDFFormat.JSONLD)
    val namedGraph = determineNamedGraph(model)
    return ConnectorModel(model, namedGraph)
  }

  @Throws(IOException::class, RDFParseException::class)
  private fun parse(connectorSelfDescriptionJsonLd: String, format: RDFFormat): Model {
    val `in` = IOUtils.toInputStream(connectorSelfDescriptionJsonLd, "UTF-8")
    return Rio.parse(`in`, "/", format)
  }

  private fun determineNamedGraph(model: Model): Resource? {
    return model.firstOrNull { statement: Statement -> subjectIsConnectorInstance(statement) }?.subject
  }

  private fun subjectIsConnectorInstance(statement: Statement): Boolean {
    return statement.predicate == RDF.TYPE &&
        (statement.getObject() == INFOMODEL.BASE_CONNECTOR || statement.getObject() == INFOMODEL.TRUSED_CONNECTOR || statement.getObject() == INFOMODEL.PARTICIPANT)
  }
}