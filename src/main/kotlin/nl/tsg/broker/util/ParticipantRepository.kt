package nl.tsg.broker.util

import de.fraunhofer.iais.eis.Participant
import nl.tsg.broker.config.BrokerConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class ParticipantRepository(private val brokerConfig: BrokerConfig, httpClient: CloseableHttpClient)
  : RepositoryProvider(httpClient) {
  @PostConstruct
  private fun postConstruct() {
    sparqlUrl = brokerConfig.participantSparqlUrl
    LOG.info("Setting SPARQL repository to be used: '$sparqlUrl'")
    for (i in 0..9) {
      try {
        repository = SPARQLRepository(sparqlUrl)
        repository!!.init()
        return
      } catch (e: Exception) {
        LOG.warn("Cannot connect to SPARQL Repository: {}", sparqlUrl)
      }
      try {
        Thread.sleep((10 * 1000).toLong())
      } catch (e: InterruptedException) {
        e.printStackTrace()
      }
    }
  }


  fun getAllParticipants() : List<Participant> {
    val query = """
      PREFIX ids: <https://w3id.org/idsa/core/>
      DESCRIBE * WHERE {
        GRAPH ?g {
          ?s ?o ?p.
        }
      }
    """.trimIndent()
    return query(query, Participant::class.java)
  }
}