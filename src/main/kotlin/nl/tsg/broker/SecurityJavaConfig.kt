package nl.tsg.broker

import nl.tsg.broker.config.BrokerConfig
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.AuthenticationException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Configuration
@EnableWebSecurity
class SecurityJavaConfig(private val brokerConfig: BrokerConfig) : WebSecurityConfigurerAdapter() {
  @Throws(Exception::class)
  override fun configure(auth: AuthenticationManagerBuilder) {
    auth.inMemoryAuthentication()
      .apply {
        brokerConfig.users.forEach{ (userName, password) ->
          withUser(userName).password(password).roles("ADMIN")
        }
      }
  }

  @Bean
  fun encoder(): PasswordEncoder {
    return BCryptPasswordEncoder()
  }

  @Throws(Exception::class)
  override fun configure(http: HttpSecurity) {
    http
      .cors()
      .and()
      .csrf()
      .disable()
      .authorizeRequests()
      .antMatchers("/api/admin/**").hasRole("ADMIN")
      .antMatchers("/**").permitAll()
      .anyRequest().permitAll()
      .and()
      .httpBasic()
      .authenticationEntryPoint(object : BasicAuthenticationEntryPoint() {
        override fun commence(
          request: HttpServletRequest,
          response: HttpServletResponse,
          authException: AuthenticationException
        ) {
          response.addHeader("WWW-Authenticate", "Basic realm=\"$realmName\"")
          response.status = HttpServletResponse.SC_UNAUTHORIZED
          response.writer.println("HTTP Status 401 - " + authException.message)
        }

        override fun afterPropertiesSet() {
          realmName = "Broker Admin Realm"
          super.afterPropertiesSet()
        }
      })
    http.addFilterAfter(
      object : GenericFilterBean() {
        override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain) {
          chain.doFilter(request, response)
        }
      },
      BasicAuthenticationFilter::class.java
    )
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(nl.tsg.broker.SecurityJavaConfig::class.java)
  }
}