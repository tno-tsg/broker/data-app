package nl.tsg.broker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["nl.tno.ids", "nl.tsg"])
@ConfigurationPropertiesScan(basePackages = ["nl.tno.ids", "nl.tsg"])
class Application

fun main(args: Array<String>) {
  runApplication<Application>(*args)
}
