package nl.tsg.broker.query

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.configuration.IdsConfig
import nl.tsg.broker.config.BrokerConfig
import nl.tsg.broker.config.FederationType
import nl.tsg.broker.federation.QueryFederation
import nl.tsg.broker.util.ConnectorRepository
import nl.tsg.broker.util.ParticipantRepository
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.URI

@Component
class QueryHandler(
  private val connectorRepository: ConnectorRepository,
  private val participantRepository: ParticipantRepository,
  private val responseBuilder: ResponseBuilder,
  private val brokerConfig: BrokerConfig,
  private val queryFederation: QueryFederation,
  private val idsConfig: IdsConfig
) : StringMessageHandler<QueryMessage> {
  private val serializationHelper = SerializationHelper.getInstance()

  override fun handle(header: QueryMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    LOG.debug("Handling query: \n{}\n{}", serializationHelper.toJsonLD(header), payload)
    var result = try {
      evaluateQuery(header.recipientScope, payload ?: "")
    } catch (e: Exception) {
      val rejectionMessage = responseBuilder.createRejectionMessage(header, RejectionReason.METHOD_NOT_SUPPORTED).build()
      val multiPartMessage = MultiPartMessage.Builder()
        .setHeader(rejectionMessage)
        .setPayload(e.message)
        .build()
      val response = MultiPart.toString(multiPartMessage, false)
      val mediaType: String = multiPartMessage.httpHeaders["Content-Type"] ?: ""
      return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaType)).body(response)
    }

    // Federate the query message to other brokers in the federation
    if (brokerConfig.federationType.contains(FederationType.QUERY_TIME) && header is QueryMessageImpl) {
      LOG.info("Query-time broker federation configured, federating {}", header::class.java)
      val responses = queryFederation.federateQuery(header, payload ?: "")
      result = queryFederation.mergeResults(result, responses)
    }

    val resultMessage = responseBuilder.createResultMessage(header)._senderAgent_(URI(idsConfig.participantId)).build()
    val multiPartMessage = MultiPartMessage.Builder()
      .setHeader(resultMessage)
      .setPayload(result)
      .build()
    val response: String = MultiPart.toString(multiPartMessage, false)
    val mediaType: String = multiPartMessage.httpHeaders["Content-Type"] ?: ""
    return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaType)).body(response)
  }

  fun evaluateQuery(target: QueryTarget, query: String): String {
    if (query.contains("DESCRIBE")) {
      // Perform SPARQL Query and return
      val result = when (target) {
        QueryTarget.BROKER -> {
          connectorRepository.query(query, Connector::class.java)
        }
        QueryTarget.PARIS -> {
          participantRepository.query(query, Participant::class.java)
        }
        else -> {
          return connectorRepository.queryPlain(query)
        }
      }
      try {
        return result.mapNotNull { toJsonLD(it) }.distinct().joinToString(separator = ", ", prefix = "[", postfix = "]")
      } catch (e: Exception) {
        LOG.error("Could not query repository:", e)
        throw e
      }
    } else {
      return connectorRepository.queryPlain(query)
    }
  }

  private fun toJsonLD(described: Described): String? {
    try {
      return SerializationHelper.getInstance().toJsonLD(described, true)
    } catch (e: IOException) {
      e.printStackTrace()
    }
    return null
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(QueryHandler::class.java)
  }
}