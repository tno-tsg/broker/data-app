package nl.tsg.broker.ui

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import de.fraunhofer.iais.eis.util.Util
import nl.tsg.broker.connectordescription.ConnectorDescriptionPersister
import nl.tsg.broker.participantdescription.ParticipantDescriptionPersister
import nl.tsg.broker.util.ConnectorRepository
import nl.tsg.broker.util.ParticipantRepository
import nl.tno.ids.common.serialization.SerializationHelper
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.IOException
import java.net.URI
import java.util.*
import java.util.function.Consumer

@RestController
@RequestMapping(value = ["/api/admin"])
@CrossOrigin("*")
class AdminController(
  private val connectorDescriptionPersister: ConnectorDescriptionPersister,
  private val participantDescriptionPersister: ParticipantDescriptionPersister,
  private val connectorRepository: ConnectorRepository,
  private val participantRepository: ParticipantRepository
) {
  @get:RequestMapping(value = ["/connectors"], method = [RequestMethod.GET])
  val connectors: List<Connector>
    get() = connectorRepository.getAllConnectors()

  @get:RequestMapping(value = ["/participants"], method = [RequestMethod.GET])
  val participants: List<Participant>
    get() = participantRepository.getAllParticipants()

  @RequestMapping(value = ["/connectors"], method = [RequestMethod.POST])
  fun addConnectors(@RequestBody connectorsString: String?): ResponseEntity<*> {
    val connectors: JSONArray = try {
      JSONArray(connectorsString)
    } catch (e: Exception) {
      LOG.info("Could not parse to jsonarray: {}", connectorsString)
      return ResponseEntity.badRequest().body("Cannot parse JSON Array")
    }
    connectors.forEach(Consumer { connector: Any ->
      try {
        connectorDescriptionPersister.ingestSelfDescription(connector.toString())
      } catch (e: IOException) {
        LOG.error("Can't ingest self description: {}", connector.toString())
      }
    })
    return ResponseEntity.ok().build<Any>()
  }

  @RequestMapping(value = ["/participants"], method = [RequestMethod.POST])
  fun addParticipants(@RequestBody participantsString: String?): ResponseEntity<*> {
    val participants = JSONArray(participantsString)
    participants.forEach(Consumer { participant: Any ->
      try {
        participantDescriptionPersister.ingestParticipantDescription(participant.toString())
      } catch (e: IOException) {
        LOG.error("Can't ingest participant description: {}", participant.toString())
      }
    })
    return ResponseEntity.ok().build<Any>()
  }

  @RequestMapping(value = ["/connectors/{connectorId}"], method = [RequestMethod.DELETE])
  fun deleteConnector(@PathVariable("connectorId") b64ConnectorId: String?): ResponseEntity<*> {
    val connectorId = String(Base64.getDecoder().decode(b64ConnectorId))
    LOG.info("Deleting connector: {}", connectorId)
    connectorDescriptionPersister.removeSelfDescription(URI.create(connectorId))
    return ResponseEntity.ok("")
  }

  @RequestMapping(value = ["/participants/{participantId}"], method = [RequestMethod.DELETE])
  fun deleteParticipant(@PathVariable("participantId") b64ParticipantId: String?): ResponseEntity<*> {
    val participantId = String(Base64.getDecoder().decode(b64ParticipantId))
    LOG.info("Deleting participant: {}", participantId)
    participantDescriptionPersister.removeParticipantDescription(participantId)
    return ResponseEntity.ok("")
  }

  @RequestMapping(value = ["/participants/{participantId}"], method = [RequestMethod.PUT])
  fun createParticipant(@PathVariable("participantId") b64ParticipantId: String?, @RequestBody participantString: String?): ResponseEntity<*> {
    val participantId = String(Base64.getDecoder().decode(b64ParticipantId))
    LOG.info("Creating participant: {}\n{}\n", participantId, participantString)
    val participant = JSONObject(participantString)
    val descriptions = ArrayList<TypedLiteral>()
    if (participant.getString("logo").isNotEmpty()) {
      descriptions.add(TypedLiteral(participant.getString("logo"), "LOGO"))
    }
    try {
      val builder = ParticipantBuilder(URI.create(participantId))
        ._title_(Util.asList(TypedLiteral(participant.getString("title"), "nl")))
        ._description_(descriptions)
        ._memberPerson_(
          Util.asList(
            PersonBuilder()
              ._givenName_(participant.getJSONObject("contact").getString("givenName"))
              ._familyName_(participant.getJSONObject("contact").getString("familyName"))
              ._emailAddress_(Util.asList(participant.getJSONObject("contact").getString("emailAddress")))
              ._phoneNumber_(Util.asList(participant.getJSONObject("contact").getString("phoneNumber")))
              .build()
          )
        )
        ._legalForm_(participant.getString("legalForm"))
        ._corporateHomepage_(URI.create(participant.getString("corporateHomepage")))
        ._corporateEmailAddress_(Util.asList(participant.getString("corporateEmailAddress")))
        ._primarySite_(SiteBuilder()._siteAddress_(participant.getString("primarySite")).build())
        .build()
      participantDescriptionPersister.ingestParticipantDescription(SerializationHelper.getInstance().toJsonLD(builder))
    } catch (e: Exception) {
      LOG.warn("Error in creating new participant:", e)
      return ResponseEntity.badRequest().body(e.message)
    }
    return ResponseEntity.ok("")
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(AdminController::class.java)
  }
}