package nl.tsg.broker.connectordescription

import nl.tsg.broker.util.ConnectorRepository
import nl.tsg.broker.util.RdfConverter
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.URI

@Component
class ConnectorDescriptionPersister(
  private val repository: ConnectorRepository,
  private val rdfConverter: RdfConverter
) {
  @Throws(IOException::class)
  fun ingestSelfDescription(connectorSelfDescription: String) {
    LOG.info("Ingesting self description")
    val result = rdfConverter.toRdf(connectorSelfDescription)
    repository.replaceStatements(result.namedGraph, result.model)
    repository.refreshCache()
  }

  fun removeSelfDescription(issuerConnector: URI) {
    LOG.info("Removing self description")
    val context = SimpleValueFactory.getInstance().createIRI(issuerConnector.toString())
    repository.replaceStatements(context)
  }

  @Throws(IOException::class)
  fun replaceSelfDescription(connectorSelfDescription: String) {
    LOG.info("Replacing self description")
    val result = rdfConverter.toRdf(connectorSelfDescription)
    repository.replaceStatements(result.namedGraph, result.model)
    repository.refreshCache()
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ConnectorDescriptionPersister::class.java)
  }
}