package nl.tsg.broker.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

enum class FederationType {
  PUBLISH_TIME, QUERY_TIME, CONNECTOR_CONTROLLED
}

@ConstructorBinding
@ConfigurationProperties
data class BrokerConfig(
  val users: Map<String, String> = HashMap(),
  val participantSparqlUrl: String?,
  val connectorSparqlUrl: String?,
  val initConnectors: List<String> = emptyList(),
  val initParticipants: List<String> = emptyList(),
  val validation: List<String> = emptyList(),
  val federatedBrokers: List<FederatedBroker> = emptyList(),
  val federationType: List<FederationType> = emptyList(),
  val uiCacheInterval: Long = 60*60*1000,
  val maxUiTimeoutSeconds: Long = 20,
  val disableUiJsonLd: Boolean = true
)