package nl.tsg.broker.federation

import de.fraunhofer.iais.eis.QueryMessageImpl
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.CoreContainerConfig
import nl.tno.ids.base.configuration.IdsConfig
import nl.tsg.broker.config.BrokerConfig
import org.apache.http.entity.ContentType
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URI

@Component
class QueryFederation(
  private val brokerConfig: BrokerConfig,
  private val idsConfig: IdsConfig,
  private val httpHelper: HttpHelper,
  private val coreContainerConfig: CoreContainerConfig
) {
  companion object {
    private val LOG = LoggerFactory.getLogger(QueryFederation::class.java)
  }

  fun federateQuery(
    incoming: QueryMessageImpl,
    payload: String
  ) = brokerConfig.federatedBrokers.mapNotNull { federatedBroker ->
    LOG.info("Federating to dataspace {}, endpoint: {}", federatedBroker.connectorId, federatedBroker.accessUrl)
    incoming.issuerConnector = URI(idsConfig.connectorId)
    incoming.senderAgent = URI(idsConfig.participantId)
    val federateResponse = httpHelper.forwardSynchronous(
      coreContainerConfig.httpsForwardEndpoint,
      federatedBroker.accessUrl,
      incoming,
      payload,
      null,
      ContentType.APPLICATION_JSON
    )
    if (federateResponse.first >= 400) {
      LOG.warn(
        "Error (Status code {}) while federating message to dataspace {}: {}",
        federateResponse.first,
        federatedBroker.connectorId,
        federateResponse.second
      )
      null
    } else {
      federateResponse.second.payload?.asString()
    }
  }

  fun mergeResults(localResult: String, federatedResponses: List<String>): String {
    return try {
      mergeConnectorArrays(localResult, federatedResponses)
    } catch (e: JSONException) {
      try {
        mergeJsonLD(localResult, federatedResponses)
      } catch (e: JSONException) {
        (listOf(localResult) + federatedResponses).joinToString(separator = "\n")
      }
    }
  }

  private fun mergeJsonLD(
    localResult: String,
    federatedResponses: List<String>
  ): String {
    val parsedLocalResult = JSONObject(localResult)
    val parsedFederatedResponses = federatedResponses.mapNotNull {
      try {
        JSONObject(it)
      } catch (e: JSONException) {
        null
      }
    }
    if (parsedLocalResult.has("@graph")) {
      mergeJsonArrays(
        parsedLocalResult.getJSONArray("@graph"),
        parsedFederatedResponses.mapNotNull {
          it.optJSONArray("@graph")
        }
      )
      mergeJsonObjects(
        parsedLocalResult.getJSONObject("@context"),
        parsedFederatedResponses.mapNotNull {
          it.optJSONObject("@context")
        }
      )
    } else {
      mergeJsonObjects(parsedLocalResult, parsedFederatedResponses)
    }
    return parsedLocalResult.toString()
  }

  private fun mergeConnectorArrays(
    localResult: String,
    federatedResponses: List<String>
  ): String {
    val parsedLocalResult = JSONArray(localResult)
    mergeJsonArrays(parsedLocalResult, federatedResponses.mapNotNull { federatedResponse ->
      try {
        JSONArray(federatedResponse)
      } catch (e: JSONException) {
        LOG.warn("Could not parse federated response")
        LOG.debug(federatedResponse)
        null
      }
    })
    return parsedLocalResult.toString()
  }

  private fun mergeJsonArrays(base: JSONArray, federated: List<JSONArray>) {
    federated.forEach { jsonArray ->
      jsonArray.forEach {
        base.put(it)
      }
    }
  }

  private fun mergeJsonObjects(base: JSONObject, federated: List<JSONObject>) {
    federated.forEach { jsonObject ->
      jsonObject.toMap().forEach { (key, value) ->
        try {
          base.putOnce(key, value)
        } catch (e: JSONException) {
          LOG.debug("Duplicate key: $key")
        }
      }
    }
  }
}