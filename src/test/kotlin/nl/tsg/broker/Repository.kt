package nl.tsg.broker

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tsg.broker.connectordescription.ConnectorDescriptionPersister
import nl.tsg.broker.participantdescription.ParticipantDescriptionPersister
import nl.tsg.broker.query.QueryHandler
import nl.tno.ids.common.serialization.SerializationHelper
import org.apache.jena.fuseki.main.FusekiServer
import org.apache.jena.query.DatasetFactory
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertTrue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.net.URI

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class RepositoryTest {
  @Autowired
  lateinit var connectorDescriptionPersister: ConnectorDescriptionPersister

  @Autowired
  lateinit var participantDescriptionPersister: ParticipantDescriptionPersister

  @Autowired
  lateinit var queryHandler: QueryHandler

  @Test
  @Order(0)
  fun ingestConnectors() {
    val connectorEndpoint1 = ConnectorEndpointBuilder()
      ._accessURL_(URI.create("https://node1.ids.smart-connected.nl"))
      .build()
    val connector1 = TrustedConnectorBuilder(URI.create("https://ids.tno.nl/connectors/TNO/TNO-Demo"))
      ._title_(arrayListOf(TypedLiteral("SCSN Customer Connector", "en")))
      ._description_(
        arrayListOf(
          TypedLiteral("Offering SCSN Customer capabilities", "en"),
          TypedLiteral("https://ids.tno.nl/scsn/type/customer", "en")
        )
      )
      ._hasEndpoint_(arrayListOf(connectorEndpoint1))
      ._hasDefaultEndpoint_(connectorEndpoint1)
      ._securityProfile_(SecurityProfile.TRUST_SECURITY_PROFILE)
      ._inboundModelVersion_(arrayListOf("2.0.0"))
      ._outboundModelVersion_("2.0.0")
      ._curator_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._maintainer_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._resourceCatalog_(
        arrayListOf(
          ResourceCatalogBuilder()
            ._offeredResource_(
              arrayListOf(
                ResourceBuilder()
                  ._title_(arrayListOf(TypedLiteral("Order response endpoint")))
                  ._resourceEndpoint_(
                    arrayListOf<ConnectorEndpoint>(
                      ConnectorEndpointBuilder()
                        ._endpointDocumentation_(arrayListOf(URI.create("https://smartconnected.t4smm.nl")))
                        ._accessURL_(connectorEndpoint1.accessURL)
                        ._path_("/")
                        .build()
                    )
                  )
                  .build()
              )
            )
            .build()
        )
      )
      .build()
    val connectorEndpoint2 = ConnectorEndpointBuilder()
      ._accessURL_(URI.create("https://node2.ids.smart-connected.nl"))
      .build()
    val connector2: Connector = TrustedConnectorBuilder(URI.create("https://ids.tno.nl/connectors/TNO/TNO-Demo2"))
      ._title_(arrayListOf(TypedLiteral("SCSN Seller Connector", "en")))
      ._description_(
        arrayListOf(
          TypedLiteral("Offering SCSN Seller capabilities", "en"),
          TypedLiteral("https://ids.tno.nl/scsn/type/seller", "en")
        )
      )
      ._hasEndpoint_(arrayListOf<ConnectorEndpoint>(connectorEndpoint2))
      ._hasDefaultEndpoint_(connectorEndpoint2)
      ._securityProfile_(SecurityProfile.TRUST_SECURITY_PROFILE)
      ._inboundModelVersion_(arrayListOf("2.0.0"))
      ._outboundModelVersion_("2.0.0")
      ._curator_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._maintainer_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._resourceCatalog_(
        arrayListOf(
          ResourceCatalogBuilder()
            ._offeredResource_(
              arrayListOf(
                ResourceBuilder()
                  ._title_(arrayListOf(TypedLiteral("Order endpoint")))
                  ._resourceEndpoint_(
                    arrayListOf<ConnectorEndpoint>(
                      ConnectorEndpointBuilder()
                        ._endpointDocumentation_(arrayListOf(URI.create("https://smartconnected.t4smm.nl")))
                        ._accessURL_(connectorEndpoint2.accessURL)
                        ._path_("/")
                        .build()
                    )
                  )
                  .build()
              )
            )
            .build()
        )
      )
      .build()
    connectorDescriptionPersister.ingestSelfDescription(SerializationHelper.getInstance().toJsonLD(connector1))
    connectorDescriptionPersister.ingestSelfDescription(SerializationHelper.getInstance().toJsonLD(connector2))
    connectorDescriptionPersister.replaceSelfDescription(
      SerializationHelper.getInstance().toJsonLD(connector1)
        .replace("https://ids.tno.nl/participants/TNO".toRegex(), "https://ids.tno.nl/participants/TNO-New")
    )
  }

  @Test
  @Order(1)
  fun ingestParticipant() {
    val participant1 = ParticipantBuilder(URI.create("https://ids.tno.nl/participants/TNO"))
      ._primarySite_(SiteBuilder()
        ._siteAddress_("Anna van Buerenplein 1, 2595 DA Den Haag").build())
      ._legalForm_("54M6")
      ._corporateHomepage_(URI.create("https://tno.nl"))
      ._corporateEmailAddress_(arrayListOf("mailto:contact@tno.nl"))
      .build()
    val participant1Json = SerializationHelper.getInstance().toJsonLD(participant1)
    val participant2 = ParticipantBuilder(URI.create("https://ids.tno.nl/participants/TNO-2"))
      ._primarySite_(SiteBuilder()
        ._siteAddress_("Anna van Buerenplein 1, 2595 DA Den Haag").build())
      ._legalForm_("54M6")
      ._corporateHomepage_(URI.create("https://tno.nl"))
      ._corporateEmailAddress_(arrayListOf("mailto:contact@tno.nl"))
      .build()
    val participant2Json = SerializationHelper.getInstance().toJsonLD(participant2)
    participantDescriptionPersister.ingestParticipantDescription(participant1Json)
    participantDescriptionPersister.ingestParticipantDescription(participant2Json)
  }

  @Test
  @Order(2)
  fun evaluateQueryReturnsConnector() {
    val result: String = queryHandler.evaluateQuery(QueryTarget.BROKER, CONNECTOR_SPARQL_QUERY)
    assertTrue(result.contains("https://ids.tno.nl/participants/TNO-New"))
  }

  @Test
  @Order(3)
  fun evaluateQueryReturnsParticipant() {
    val result: String = queryHandler.evaluateQuery(QueryTarget.PARIS, PARTICIPANT_SPARQL_QUERY)
    assertTrue(result.contains("https://ids.tno.nl/participants/TNO-2"))
  }


  @Test
  @Order(4)
  fun ingestConnectorUpdate() {
    val connectorEndpoint1 = ConnectorEndpointBuilder()
      ._accessURL_(URI.create("https://node1.ids.smart-connected.nl"))
      .build()
    val connector1 = TrustedConnectorBuilder(URI.create("https://ids.tno.nl/connectors/TNO/TNO-Demo"))
      ._title_(arrayListOf(TypedLiteral("SCSN Customer Connector", "en")))
      ._description_(
        arrayListOf(
          TypedLiteral("Offering SCSN Customer capabilities", "en"),
          TypedLiteral("https://ids.tno.nl/scsn/type/customer", "en")
        )
      )
      ._hasEndpoint_(arrayListOf(connectorEndpoint1))
      ._hasDefaultEndpoint_(connectorEndpoint1)
      ._securityProfile_(SecurityProfile.TRUST_SECURITY_PROFILE)
      ._inboundModelVersion_(arrayListOf("2.0.0"))
      ._outboundModelVersion_("2.0.0")
      ._curator_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._maintainer_(URI.create("https://ids.tno.nl/participants/TNO"))
      ._resourceCatalog_(
        arrayListOf(
          ResourceCatalogBuilder()
            ._offeredResource_(
              arrayListOf(
                ResourceBuilder()
                  ._title_(arrayListOf(TypedLiteral("Order response endpoint")))
                  ._resourceEndpoint_(
                    arrayListOf<ConnectorEndpoint>(
                      ConnectorEndpointBuilder()
                        ._endpointDocumentation_(arrayListOf(URI.create("https://smartconnected.t4smm.nl")))
                        ._accessURL_(connectorEndpoint1.accessURL)
                        ._path_("/")
                        .build()
                    )
                  )
                  .build()
              )
            )
            .build()
        )
      )
      .build()

    connectorDescriptionPersister.replaceSelfDescription(SerializationHelper.getInstance().toJsonLD(connector1))

    println()
  }

  companion object {
    @BeforeAll
    @JvmStatic
    fun beforeClass() {
      val server = FusekiServer.create()
        .add("/connectorData", DatasetFactory.createTxnMem())
        .add("/participantData", DatasetFactory.createTxnMem())
        .enableMetrics(false)
        .port(0)
        .build()
      server.start()
      System.setProperty("connectorSparqlUrl", "http://localhost:${server.httpPort}/connectorData")
      System.setProperty("participantSparqlUrl", "http://localhost:${server.httpPort}/participantData")
    }

    private val CONNECTOR_SPARQL_QUERY =
      """
      PREFIX ids: <https://w3id.org/idsa/core/>
        DESCRIBE * WHERE {
          GRAPH ?g {
            ?s ?o ?p.
          }
        }
      """.trimIndent()
    private const val PARTICIPANT_SPARQL_QUERY = "PREFIX ids: <https://w3id.org/idsa/core/>\n" +
        "DESCRIBE * WHERE {\n" +
        "  GRAPH ?g { \n" +
        "    ?s ?o ?p.\n" +
        "  }\n" +
        "}\n"
  }
}