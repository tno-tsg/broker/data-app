# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.3.0] - 30-01-2024
### Changed
- Added caching of connectors for UI calls


## [4.2.0] - 17-04-2023
### Changed
- Removed Testcontainers dependency to Fuseki service, now using an embedded Fuseki for testing
- Dependency updates:
  - Spring 2.4.x > 2.7.10
  - RDF4J 2.4.3 > 4.2.3
  - JSON 20180813 > 20230227
  - Kotlin 1.5.31 > 1.8.20