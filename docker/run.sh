#!/bin/sh

echo "Starting Spring boot app"

# Set a server port, default is 8080
: ${SERVER_PORT=8080}


ARGS="-Djava.security.egd=file:/dev/./urandom -Dserver.port=${SERVER_PORT}"

# Start suspended in debug mode
if [ -n "${JAVA_DEBUG_PORT}" ] ; then
    ARGS="${ARGS} -Xdebug -Xrunjdwp:transport=dt_socket,address=${JAVA_DEBUG_PORT},server=y,suspend=y"
fi

# Add proxy args
if [ ! -z "$PROXY_HOST" ]; then
    ARGS="${ARGS} -Dhttp.proxyHost=${PROXY_HOST} -Dhttp.proxyPort=${PROXY_PORT}"
    if [ ! -z "$PROXY_USER" ]; then
        ARGS="${ARGS} -Dhttp.proxyUser=${PROXY_USER}"
    fi
    if [ ! -z "$PROXY_PASS" ]; then
        ARGS="${ARGS} -Dhttp.proxyPassword=${PROXY_PASS}"
    fi
fi

echo "ARGS=${ARGS}"

exec java -jar -Xms128m -Xmx128m -Dfile.encoding=UTF-8 ${ARGS} ${JAVA_ARGS} /app/ids-app.jar $@
